#![feature(nll)]
#![feature(int_to_from_bytes)]
#![feature(if_while_or_patterns)]
extern crate clap;
extern crate base64;
extern crate serde;
extern crate serde_cbor;
#[macro_use]
extern crate serde_derive;
extern crate serde_bytes;
extern crate petgraph;
extern crate rusty_secrets;
extern crate blake2;
extern crate ansi_term;

use std::process::exit;
use std::fs::{File,OpenOptions};
use std::io::{Read, Write};
use std::path::PathBuf;
use std::io;
use std::fs;
use std::ffi::OsString;

use blake2::{Blake2s, Digest};
use petgraph::graph::{NodeIndex, DiGraph};
use petgraph::Direction::Outgoing;
use petgraph::visit::EdgeRef;
use petgraph::dot::{Config,Dot};
use std::fmt::{Debug, Display};
use rusty_secrets::sss::{split_secret, recover_secret};
use base64::{encode_config, decode_config, STANDARD_NO_PAD};
use serde_bytes::ByteBuf;
use clap::{Arg, App, SubCommand, AppSettings};
use ansi_term::Color::Red;

type Graph = DiGraph<DataElement, u8>;

fn main() {
    let matches = App::new("secret_splitter")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .version("1.0")
        .author("Steven Xu <stevendoesstuffs@protonmail.com>")
        .about("Takes your secret and splits in in fancy ways! Will produce parts roughly equal in size to the secret. Keep the tree depth, n, k, and total number of parts under 255.")
        .arg(Arg::with_name("replace")
            .short("r")
            .long("replace")
            .help("Auto respond yes to override prompts. Program will not require interaction if present.")
            .takes_value(false)
            .required(false))
        .subcommand(SubCommand::with_name("split")
            .about("Splits your secret")
            .arg(Arg::with_name("pattern")
                .short("p")
                .long("pattern")
                .value_name("PATTERN")
                .help("Splitting pattern. See gitlab for more info.")
                .takes_value(true)
                .required(true))
            .arg(Arg::with_name("input")
                .short("i")
                .long("input")
                .value_name("FILE")
                .help("File to split.")
                .takes_value(true)
                .required(true))
            .arg(Arg::with_name("output")
                .short("o")
                .long("output")
                .value_name("FOLDER")
                .help("Output parts to this folder.")
                .takes_value(true)
                .required(true))
            .arg(Arg::with_name("dot")
                .short("d")
                .long("dot")
                .value_name("FILE")
                .help("Output file of dot graph.")
                .takes_value(true)
                .required(false)))
        .subcommand(SubCommand::with_name("recover")
            .about("Recovers your secret from parts")
            .arg(Arg::with_name("input")
                .short("i")
                .long("input")
                .value_name("FOLDER")
                .help("Folder where parts are contained.")
                .takes_value(true)
                .required(true))
            .arg(Arg::with_name("output")
                .short("o")
                .long("output")
                .value_name("FILE")
                .help("Output file.")
                .takes_value(true)
                .required(true))
            .arg(Arg::with_name("dot")
                .short("d")
                .long("dot")
                .value_name("FILE")
                .help("Output file of dot graph.")
                .takes_value(true)
                .required(false))).get_matches();

    let replace = matches.is_present("replace");
    match matches.subcommand() {
        ("split", Some(submatches)) => {
            if let Ok(mut file) = File::open(submatches.value_of("input").unwrap()) {
                let pattern = submatches.value_of("pattern").unwrap();

                let mut tree = build_from_input(pattern);
                let mut graph = Graph::new();

                let mut vec = Vec::new();
                if let Err(e) = file.read_to_end(&mut vec) {
                    error(format!("{}", e), true);
                }

                let hash = Blake2s::new()
                    .chain(vec.as_slice())
                    .result();

                let new_node = graph.add_node(DataElement::Root(tree.k, tree.n, Some(ByteBuf::from(vec))));
                build_into_graph(&mut tree, &mut graph, new_node);

                if let Some(p) = submatches.value_of("dot") {
                    let p = PathBuf::from(p);
                    if p.is_file() {
                        if replace || prompt_yes(format!("Override file at \"{}\" (y/n)?", p.display())) {
                            if let Err(e) = fs::write(&p, format!("{:?}", Dot::with_config(&graph, &[Config::EdgeNoLabel]))) {
                                error(format!("Error while reading or writing to path (failed to write dot file, skipping) \"{}\"\n{}", Red.bold().paint("error"), e), false);
                            }
                        } else {
                            println!("info: Skipping dot file.");
                        }
                    } else if p.is_dir() {
                        error(format!("Cannot write dot file to folder, skipping: {}", p.display()), false);
                    } else {
                        if let Err(e) = fs::write(&p, format!("{:?}", Dot::with_config(&graph, &[Config::EdgeNoLabel]))) {
                            error(format!("Error while reading or writing to path (failed to write dot file, skipping) \"{}\"\n{}", Red.bold().paint("error"), e), false);
                        }
                    }
                }

                create_data(&mut graph, new_node);
                let tree_vec = serde_cbor::to_vec(&tree).unwrap();

                let mut path = PathBuf::from(submatches.value_of("output").unwrap());
                
                write_data(&mut graph, new_node, tree_vec.as_slice(), &mut path, &mut Vec::new(), hash.as_slice(), replace);                

            } else {
                error("Error reading input file!", true);
            }
        } ("recover", Some(submatches)) => {
            let input = PathBuf::from(submatches.value_of("input").unwrap());
            let output = PathBuf::from(submatches.value_of("output").unwrap());
            if !input.is_dir() {
                error("Cannot read parts from non-directory", true);
            }
            let mut paths = Vec::new();
            traverse_dir(&input, &mut paths);

            let mut first: Option<(Vec<u8>, PathBuf)> = None;

            let mut data = Vec::new();

            for path in paths {
                if let Ok(vec) = fs::read(&path) {
                    if let Some((ref first_v, ref first_path)) = first {
                        if vec[0..32] != first_v[0..32] || 
                                vec[36..(36 + u32::from_le_bytes([vec[32],vec[33],vec[34],vec[35]]) as usize)] != 
                                first_v[36..(36 + u32::from_le_bytes([first_v[32],first_v[33],first_v[34],first_v[35]]) as usize)] {
                            error(format!("Parts in folder belong to multiple files! File 1: \"{}\", File 2: \"{}\"", &path.display(), first_path.display()), true);
                        }
                    } else {
                        first = Some((vec.clone(), path.clone()));
                    }
                    if let Some(a) = split_data(vec, &path) {
                        data.push(a);
                    }
                }
            }

            if data.len() == 0 || data.len() == 1 {
                error(format!("Not enough parts found in input folder. Check to make sure permissions are set correctly. Number of parts found: {}", data.len()), true);
            }
            let first = first.unwrap().0;
            let tree = match serde_cbor::from_slice::<Tree>(&first[36..(36 + u32::from_le_bytes([first[32],first[33],first[34],first[35]]) as usize)]) {
                Ok(t) => t,
                Err(e) => {
                    error(format!("Couldn't serialize tree. Data likely corrupt.\n{}", e), true);
                    Tree::default()
                }
            };

            let mut graph = Graph::new();
            let new_node = graph.add_node(DataElement::Root(tree.k, tree.n, None));
            build_into_graph(&tree, &mut graph, new_node);
            
            for mut e in data {
                add_node(&mut graph, &mut e.0, &mut e.1, new_node);
            }

            let build = build_tree(&mut graph, new_node);

            if let Some(p) = submatches.value_of("dot") {
                let p = PathBuf::from(p);
                if p.is_file() {
                    if replace || prompt_yes(format!("Override file at \"{}\" (y/n)?", p.display())) {
                        if let Err(e) = fs::write(&p, format!("{:?}", Dot::with_config(&graph, &[Config::EdgeNoLabel]))) {
                            error(format!("Error while reading or writing to path (failed to write dot file, skipping) \"{}\"\n{}", Red.bold().paint("error"), e), false);
                        }
                    } else {
                        println!("info: Skipping dot file.");
                    }
                } else if p.is_dir() {
                    error(format!("Cannot write dot file to folder, skipping: {}", p.display()), false);
                } else {
                    if let Err(e) = fs::write(&p, format!("{:?}", Dot::with_config(&graph, &[Config::EdgeNoLabel]))) {
                        error(format!("Error while reading or writing to path (failed to write dot file, skipping) \"{}\"\n{}", Red.bold().paint("error"), e), false);
                    }
                }
            }

            if build {
                if let DataElement::Root(_,_,ref v) = graph.node_weight(new_node).unwrap() {
                    if output.is_file() && !(replace || prompt_yes(format!("Override file at \"{}\" (y/n)?", output.display()))) {
                        let answer = prompt_answer("New place to output, answer exit() to exit (FILE): ");
                        if answer == "exit()" {
                            println!("Exiting...");
                            exit(0);
                        } else {
                            let output = PathBuf::from(answer);
                            handle_result(fs::write(&output, v.as_ref().unwrap()),&output);
                        }
                    } else if output.is_dir() {
                        error(format!("Cannot override directory: {}", output.display()), true);
                    } else {
                        handle_result(fs::write(&output, v.as_ref().unwrap()),&output);
                    }
                } else {
                    error("An unknown error has occurred with the program.", true);
                }
            } else {
                error("Failed to piece back the secret for unknown reason; it may be that you don't have enough parts.", true);
            }
        } _ => {}
    }
}

fn error<T: AsRef<str>>(message: T, fatal: bool) {
    let e = if fatal {
        "fatal"
    } else {
        "error"
    };
    eprintln!("{}: {}", Red.bold().paint(e), message.as_ref());
    if fatal {
        exit(1);
    }
}

fn build_tree(g: &mut Graph, node: NodeIndex) -> bool {
    let weight = g.node_weight(node).unwrap();
    let mut build = Vec::new();
    let mut vec = Vec::new();
    let mut data = None;
    let mut k1 = 0;
    match weight {
        &DataElement::MandatoryNested(k,_,None) | &DataElement::Nested(k,_,None) | &DataElement::Root(k,_,None) => {
            for edge in g.edges_directed(node, Outgoing) {
                let index = edge.target();
                match g.node_weight(index).unwrap() {
                    &DataElement::MandatoryNested(_,_,_) => {
                        build.push((*edge.weight(),index, true));
                    } &DataElement::Nested(_,_,_) => {
                        build.push((*edge.weight(),index, false));
                    } &DataElement::Mandatory(ref v) => {
                        if let &None = v {
                            return false;
                        }
                        vec.push((true, *edge.weight(),index));
                    } &DataElement::Normal(ref v) => {
                        if let &Some(_) = v {
                            vec.push((false, *edge.weight(),index));
                        }
                    } _ => {}
                }
            }
            k1 = k;
        } _ => {
            error("Cannot build tree on element or tree with data.", true);
        }
    }

    for index in build {
        if build_tree(g, index.1) {
            vec.push((index.2, index.0,index.1));
        } else {
            if index.2 {
                return false;
            }
        }
    }

    if vec.len() < k1 as usize {
        return false;
    }
    let mut parts_strings = Vec::new();
    let mut man_key = None;
    for (m, _, n) in &vec {
        if *m {
            match g.node_weight(*n).unwrap() {
                &DataElement::MandatoryNested(_,_,ref v) | &DataElement::Nested(_,_,ref v) | &DataElement::Mandatory(ref v) | &DataElement::Normal(ref v) | &DataElement::Root(_,_, ref v) => {
                    if man_key == None {
                        man_key = Some(v.as_ref().unwrap().clone());
                    } else {
                        xor_into(man_key.as_mut().unwrap(), v.as_ref().unwrap());
                    }

                }
            };
        }
    }


    for (_, w, n) in vec {
        let v = match g.node_weight_mut(n).unwrap() {
            &mut DataElement::MandatoryNested(_,_,ref v) | &mut DataElement::Mandatory(ref v) => {
                v.as_ref().unwrap()
            } &mut DataElement::Nested(_,_,ref mut v) | &mut DataElement::Normal(ref mut v) | &mut DataElement::Root(_,_, ref mut v) => {
                if let Some(ref v2) = man_key {
                    xor_into(v.as_mut().unwrap(), v2);
                }
                v.as_ref().unwrap()
            }
        };
        parts_strings.push(format!("{}-{}-{}", k1, w + 1, encode_config(v, STANDARD_NO_PAD)));
    }
    match recover_secret(parts_strings.as_slice(), false) {
        Ok(v) => data = Some(v),
        Err(_) => {
            return false
        }
    }

    if data == None {
        return false;
    }

    let d = data.unwrap();
    match g.node_weight_mut(node).unwrap() {
        &mut DataElement::MandatoryNested(_,_,ref mut v) | 
                &mut DataElement::Nested(_,_,ref mut v) | 
                &mut DataElement::Root(_,_,ref mut v) => {
            *v = Some(ByteBuf::from(d));
        } _ => {
            error("Cannot build tree on element or tree with existing data.", true);
        }
    };
    true
}

fn add_node(g: &mut Graph, travel: &mut Vec<u8>, data: &mut Vec<u8>, root: NodeIndex) {
    let mut index = root;
    loop {
        let i = travel.remove(0);
        let mut new_index = root;
        for edge in g.edges_directed(index, Outgoing) {
            if *edge.weight() == i {
                new_index = edge.target();
            }
        }
        if new_index == root {
            error("Travel invalid on one of the parts, skipping.", false);
            return;
        }
        if travel.len() > 0 {
            index = new_index;
        } else {
            match g.node_weight_mut(new_index).unwrap() {
                &mut DataElement::Mandatory(ref mut buf) | &mut DataElement::Normal(ref mut buf) => {
                    *buf = Some(ByteBuf::from(data.to_owned()));
                    break;
                } _ => {
                    error("Travel invalid on one of the parts, skipping.", false);
                    return;
                }
            }
        }
    }
}

fn split_data(mut v: Vec<u8>, path: &PathBuf) -> Option<(Vec<u8>, Vec<u8>)> {
    let b32 = 32_u32.to_le_bytes();
    for i in 0..4 {
        v.insert(i,b32[i]);
    }
    try_split(&mut v, path);
    try_split(&mut v, path);

    let a = match try_split(&mut v, path) {
        Some(s) => s,
        _ => return None
    };

    let b = match try_split(&mut v, path) {
        Some(s) => s,
        _ => return None
    };

    if v.len() != 0 {
        error(format!("File too long at \"{}\", continuing without it.", path.display()), false);
        return None;
    }
    Some((a,b))
}

fn try_split(v: &mut Vec<u8>, path: &PathBuf) -> Option<Vec<u8>> {
    if v.len() < 4 {
        error(format!("Corrupt file: \"{}\", continuing without it.", path.display()), false);
        return None;
    }
    let arr = [v.remove(0),v.remove(0),v.remove(0),v.remove(0)];
    let a = u32::from_le_bytes(arr) as usize;
    if v.len() < a {
        error(format!("Corrupt file: \"{}\", continuing without it.", path.display()), false);
        return None;
    }
    let half_2 = v.split_off(a);
    let half_1 = v.clone();
    *v = half_2;
    Some(half_1)
}

fn traverse_dir(path: &PathBuf, v: &mut Vec<PathBuf>) {
    for entry in handle_result(fs::read_dir(path), path) {
        if entry.is_err() {
            continue;
        }
        let entry = entry.unwrap().path();
        let path_oss = OsString::from("part");
        if entry.is_file() && entry.extension() == Some(path_oss.as_os_str()) {
            v.push(entry);
        } else if entry.is_dir() {
            traverse_dir(&entry, v);
        }
    }
}


fn write_data(g: &Graph, node: NodeIndex, t: &[u8], path: &mut PathBuf, travel: &mut Vec<u8>, id: &[u8], replace: bool) {
    match g.node_weight(node).unwrap() {
        &DataElement::Root(_, _, Some(_)) | &DataElement::MandatoryNested(_, _, Some(_)) | &DataElement::Nested(_, _, Some(_)) => {
            create_folder(path);

            for element in g.edges_directed(node, Outgoing) {
                let mut s = element.weight().to_string();
                if let &DataElement::Mandatory(_) | &DataElement::MandatoryNested(_,_,_) = g.node_weight(element.target()).unwrap() {
                    s.insert(0,'m');
                }
                travel.push(*element.weight());
                path.push(s);
                write_data(g, element.target(), t, path, travel, id, replace);
            }

        } &DataElement::Normal(Some(ref buf)) | &DataElement::Mandatory(Some(ref buf)) => {
            path.set_extension("part");
            if path.is_dir() {
                error(format!("Cannot override directory: {}", path.display()), true);
            } else if path.is_file() {
                if !(replace || prompt_yes(format!("Override file at \"{}\" (y/n)?", path.display()))) {
                    println!("Exiting...");
                    exit(0);
                }
            }
            write(path, t, travel, buf, id);
            path.set_extension("");
        } _ => {
            error("Unknown error occurred while writing data (empty buffer)", true);
        }
    }
    path.pop();
    travel.pop();
}

fn create_folder(path: &PathBuf) {
    if !path.exists() {
        if let Err(e) = fs::create_dir_all(&path) {
            error(format!("{}: Error while creating folder at path \"{}\"\n{}", Red.bold().paint("error"), path.display(), e), true);
        }     
    }
    else if path.is_file() {
        error(format!("Cannot override file (expected directory): {}", path.display()), true);
    }
}

fn write(path: &PathBuf, t: &[u8], travel: &[u8], buf: &[u8], id: &[u8]) {
    let mut file = handle_result(OpenOptions::new().read(true).write(true).append(true).create_new(true).open(path), path);
    handle_result(file.write(id), path);
    handle_result(file.write(&(t.len() as u32).to_le_bytes()), path);
    handle_result(file.write(t), path);
    handle_result(file.write(&(travel.len() as u32).to_le_bytes()), path);
    handle_result(file.write(travel), path);
    handle_result(file.write(&(buf.len() as u32).to_le_bytes()), path);
    handle_result(file.write(buf), path);
}

fn handle_result<T>(result: io::Result<T>, path: &PathBuf) -> T {
    if let &Err(ref e) = &result {
        error(format!("{}: Error while reading or writing to path \"{}\"\n{}", Red.bold().paint("fatal"), path.display(), e), true);
    }
    result.unwrap()
}

fn is_yes<T: AsRef<str>>(s: T) -> bool {
    let s = s.as_ref().trim();
    return s == "y" || s == "yes" || s == "Y" || s == "YES";
}

fn prompt_yes<T: AsRef<str>>(prompt: T) -> bool {
    println!("asdf");
    let prompt = prompt.as_ref();
    print!("{}", prompt);
    let mut s = String::new();
    io::stdin().read_line(&mut s).unwrap();
    return is_yes(s);
}

fn prompt_answer<T: AsRef<str>>(prompt: T) -> String {
    let prompt = prompt.as_ref();
    print!("{}", prompt);
    let mut s = String::new();
    io::stdin().read_line(&mut s).unwrap();
    return s;
}


fn create_data(g: &mut Graph, tree: NodeIndex) {
    let node = &g[tree];
    match node {
        &DataElement::Mandatory(_) | &DataElement::Normal(_) => {
            error("Create data called on non-tree! This is an error with the program, please open an issue on gitlab.", true);
        } &DataElement::Nested(k, n, ref v) | &DataElement::MandatoryNested(k, n, ref v) | &DataElement::Root(k, n, ref v)  => {
            if let &Some(ref v) = v {
                if let Ok(mut shares) = split_secret(k, n, v, false) {
                    let mut shares_bytes = Vec::<Vec<u8>>::new();
                    while let Some(share) = shares.pop() {
                        shares_bytes.push(share_to_bytes(share));
                    }
                    let mut neighbors = g.neighbors_directed(tree, Outgoing).detach();
                    let mut xor = Vec::<u8>::new();
                    while let Some((_, n_index)) = neighbors.next(g) {
                        let n = g.node_weight_mut(n_index).unwrap();
                        let share = shares_bytes.remove(0);
                        match n {
                            &mut DataElement::Mandatory(ref mut v) | &mut DataElement::MandatoryNested(_, _, ref mut v) => {
                                if xor.len() == 0 {
                                    xor = share.clone();
                                } else {
                                    xor_into(xor.as_mut_slice(), share.as_slice());
                                }
                                *v = Some(ByteBuf::from(share));
                            } &mut DataElement::Nested(_, _, ref mut v) | &mut DataElement::Normal(ref mut v) | &mut DataElement::Root(_, _, ref mut v)  => {
                                *v = Some(ByteBuf::from(share));
                            }
                        }
                    }
                    let mut neighbors = g.neighbors_directed(tree, Outgoing).detach();
                    while let Some((_, n_index)) = neighbors.next(g) {
                        let n = g.node_weight_mut(n_index).unwrap();
                        match n {
                            &mut DataElement::Normal(ref mut v) => if xor.len() != 0 { xor_into(v.as_mut().unwrap(), xor.as_slice()) },
                            &mut DataElement::Nested(_, _, ref mut v) => {
                                if xor.len() != 0 { xor_into(v.as_mut().unwrap(), xor.as_slice()); }
                                create_data(g, n_index);
                            }
                            &mut DataElement::MandatoryNested(_, _, _) | &mut DataElement::Root(_, _, _) => {
                                create_data(g, n_index);
                            }
                            _ => {}
                        }
                    }
                }
            } else {
                error("Tree contains no data! This is an error with the program, please open an issue on gitlab.", true);
            }
        } 
    }
}

fn xor_into(a: &mut [u8], b: &[u8]) {
    if a.len() != b.len() {
        error("Cannot xor vectors with differing lengths! This is likely an issue with the program, please report on gitlab.", true);
    }
    for i in 0..a.len() {
        a[i] = a[i] ^ b[i];
    }
}

fn share_to_bytes(s: String) -> Vec<u8> {
    decode_config(s.split('-').last().unwrap(), STANDARD_NO_PAD).unwrap()
}

fn build_into_graph(t: &Tree, g: &mut Graph, parent: NodeIndex) {
    let mut index = 0;
    for node in &t.nodes {
        match node {
            &Node::Branch(ref tree) => {
                let p = g.add_node(DataElement::Nested(tree.k, tree.n, None));
                g.update_edge(parent, p, index);
                build_into_graph(tree, g, p);
                index += 1;
            } &Node::MandatoryBranch(ref tree) => {
                let p = g.add_node(DataElement::MandatoryNested(tree.k, tree.n, None));
                g.update_edge(parent, p, index);
                build_into_graph(tree, g, p);
                index += 1;
            } &Node::Leaf(ref ele) => {
                match *ele {
                    Elements::Normal(n) => for _i in 0..n {
                        let new_node = g.add_node(DataElement::Normal(None));
                        g.update_edge(parent, new_node, index);
                        index += 1;
                    } Elements::Mandatory(n) => for _i in 0..n {
                        let new_node = g.add_node(DataElement::Mandatory(None));
                        g.update_edge(parent, new_node, index);
                        index += 1;
                    }
                }
            }
        }
    }
}

fn build_from_input(s: &str) -> Tree {
    let s = &s.to_string().replace(" ", "");
    if !s.is_ascii() { error("Input must be ASCII.", true); }
    let valid_chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '{', '}', '[', ']', '@', 'm', ','];
    for (i, c) in s.char_indices() {
        if !valid_chars.contains(&c) {
            error(format!("{}: Input contains invalid character '{}' at {}: {}\n{:width$}^", Red.bold().paint("fatal"), c, i, s, "", width = i + 50), true);
        }
    }
    let mut tree = recursive_build_tree(s, 0, s);
    transform_tree(&mut tree);
    tree
}

fn transform_tree(tree: &mut Tree) {
    let vector = &mut tree.nodes;
    let mut normal = 0;
    let mut mandatory = 0;
    let mut i = Some(vector.len() - 1);
    let mut m_branch = 0;

    while i != None {
        match *vector.get_mut(i.unwrap()).unwrap() {
            Node::Branch(ref mut tree) => {
                transform_tree(tree);
            } Node::MandatoryBranch(ref mut tree) => {
                m_branch += 1;
                transform_tree(tree);
            } Node::Leaf(ref element) => {
                match element {    
                    Elements::Normal(ref num) => {
                        normal += *num;
                        vector.remove(i.unwrap());
                    } Elements::Mandatory(ref num) => {
                        mandatory += *num;
                        vector.remove(i.unwrap());
                    }
                }
            }
        }
        let u = i.unwrap();
        if u == 0 { i = None; } else {
            i = Some(u - 1);
        }
    }
    if m_branch + mandatory >= tree.k {
        error("Cannot have more than k mandatory parts in tree!", true);
    }
    if vector.len() as u8 + normal + mandatory > tree.n {
        error("Too many elements specified in one or more lists!", true);
    }
    let new_normal = tree.n - (mandatory + vector.len() as u8);
    vector.push(Node::Leaf(Elements::Mandatory(mandatory)));
    vector.push(Node::Leaf(Elements::Normal(new_normal)));
}


#[derive(Serialize, Deserialize)]
enum DataElement { 
    Normal(Option<ByteBuf>),
    Mandatory(Option<ByteBuf>),
    Nested(u8, u8, Option<ByteBuf>),
    MandatoryNested(u8, u8, Option<ByteBuf>),
    Root(u8, u8, Option<ByteBuf>)
}

impl Display for DataElement {
     fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            DataElement::Normal(_) => write!(f, "Normal"),
            DataElement::Mandatory(_) => write!(f, "Mandatory"),
            DataElement::Nested(k, n, _) => write!(f, "Nested: {}, {}", k, n),
            DataElement::MandatoryNested(k, n, _) => write!(f, "Mandatory Nested: {}, {}", k, n),
            DataElement::Root(k, n, _) => write!(f, "Root: {}, {}", k, n),
        }
    }
}

impl Debug for DataElement {
     fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            DataElement::Normal(v) => write!(f, "Normal ({})", if v.is_some() { "Y" } else { "N" }),
            DataElement::Mandatory(v) => write!(f, "Mandatory ({})", if v.is_some() { "Y" } else { "N" }),
            DataElement::Nested(k, n, v) => write!(f, "Nested: {}, {} ({})", k, n, if v.is_some() { "Y" } else { "N" }),
            DataElement::MandatoryNested(k, n, v) => write!(f, "Mandatory Nested: {}, {} ({})", k, n, if v.is_some() { "Y" } else { "N" }),
            DataElement::Root(k, n, v) => write!(f, "Root: {}, {} ({})", k, n, if v.is_some() { "Y" } else { "N" }),
        }
    }
}

#[derive(Serialize, Deserialize)]
enum Node {
    Branch(Tree),
    MandatoryBranch(Tree),
    Leaf(Elements)
}

#[derive(Serialize, Deserialize, Default)]
struct Tree {
    nodes: Vec<Node>,
    k: u8,
    n: u8
}

impl Tree {
    fn new(nodes: Vec<Node>, k: u8, n: u8) -> Self {
        Tree {
            nodes,
            k,
            n
        }
    }
}

#[derive(Serialize, Deserialize)]
enum Elements {
    Normal(u8),
    Mandatory(u8)
}


impl Debug for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Node::Leaf(ref element) => {
                match element {
                    Elements::Normal(ref num) => {
                        write!(f, "{}", *num)
                    } Elements::Mandatory(ref num) => {
                        write!(f, "m{}", *num)
                    }
                }
            } Node::Branch(ref tree) => {
                write!(f, "{:?}", tree)
            } Node::MandatoryBranch(ref tree) => {
                write!(f, "m{:?}", tree)
            }
        }

    }
}

impl Debug for Tree {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{{{}, {}}}{:?}", self.k, self.n, self.nodes)
    }
}



fn recursive_build_tree(s: &str, start: usize, orig: &str) -> Tree {
    let (k,n,p) = kn(s, start, orig);
    if k > n {
        error(format!("{}: k can't be greater than n: {}\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = 34 + start), true);
    }
    let s2 = ss(s, p + 1, s.len(), start, orig);
    if s2 == "@" {
        Tree::new(vec![Node::Leaf(Elements::Normal(n))], k, n)
    } else if ss(s2, 0, 1, start, orig) == "[" {
        let start = start + p + 1;
        let end = find_pair(s2, 0, start, orig);
        if end != s2.len() - 1 {
            error(format!("{}: Excess input after list: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, width = 33 + start), true);
        }

        let start = start + 1;
        let list = ss(s2,1,s2.len() - 1, start, orig);
        let mut ele_vec = Vec::<(usize, &str)>::new();
        let mut begin = 0;
        let mut iter: Box<std::iter::FusedIterator<Item = (usize, char)>> = Box::new(list.char_indices());

        loop {
            match iter.next() {
                Some((i,c)) => {
                    if c == '{' { 
                        let pair = find_pair(list, i, start, orig);
                        match list.get(pair + 1..pair + 2) {
                            Some("@") => {
                                match list.get(pair + 2..pair + 3) {
                                    Some(",") | None => {
                                        iter = Box::new(iter.skip(pair - i + 2));
                                        ele_vec.push((start + begin, ss(list, begin, pair + 2, start, orig)));
                                        begin = pair + 3;
                                    } _ => {
                                        error(format!("{}: Expected either \"@\" or list: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + pair + 38), true);
                                    }
                                }
                            }
                            _ => {
                                match try_find_pair(list, pair + 1) {
                                    Ok(pair2) => {
                                        iter = Box::new(iter.skip(pair2 - i + 1));
                                        ele_vec.push((start + begin, ss(list, begin, pair2 + 1, start, orig)));
                                        begin = pair2 + 2;
                                    } Err(e) => {
                                        if e == 0 {
                                            error(format!("{}: Expected either \"@\" or list: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + pair + 38), true);
                                        }
                                        find_pair(list, pair + 1, start, orig);
                                    }
                                }
                            }
                        }
                    }
                    if c == ',' {
                        ele_vec.push((start + begin, ss(list, begin, i, start, orig)));
                        begin = i + 1;
                    }
                },
                None => {
                    if begin < list.len() {
                        ele_vec.push((start + begin, ss(list, begin, list.len(), start, orig)));
                    }
                    break;
                }
            }
        }
        let mut node_vec = Vec::<Node>::new();

        for (start, ele) in ele_vec {
            node_vec.push(recursive_build_node(ele, start, orig));
        }

        Tree::new(node_vec, k, n)
    } else {
        error(format!("{}: Expected either \"@\" or list: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + p + 38), true);
        Tree::default()
    }
}

fn recursive_build_node(s: &str, start: usize, orig: &str) -> Node {
    let first = ss(s, 0, 1, start, orig);
    if let Ok(num) = s.parse::<u8>() {
        return Node::Leaf(Elements::Normal(num));
    } else if first == "m" || first == "M" {
        match s.get(1..s.len()) {
            None | Some("") => {
                return Node::Leaf(Elements::Mandatory(1));
            }
            Some(n) => {
                if let Ok(num) = (&s[1..s.len()]).parse::<u8>() {
                    return Node::Leaf(Elements::Mandatory(num));
                } else if &n[0..1] == "{" {
                    return Node::MandatoryBranch(recursive_build_tree(&s[1..s.len()], start + 1, orig));
                } else {
                    error(format!("{}: Expected node: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + 23), true);
                }
            }
        }
    } else if first == "{" {
        return Node::Branch(recursive_build_tree(s, start, orig));
    } else {
        error(format!("{}: Expected node: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + 23), true);
    }
    Node::Leaf(Elements::Normal(0))
}

fn kn(s: &str, start: usize, orig: &str) -> (u8, u8, usize) {
    if ss(s, 0, 1, start, orig) != "{" {
        error(format!("{}: List must begin with {{k,n}}: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + 36), true);
    }
    let p = find_pair(s, 0, start, orig);
    let mut count = 0;
    let mut k = 0;
    let mut n = 0;
    for ele in ss(s, 1, p, start, orig).split(",") {
        if let Ok(num) = ele.parse::<u8>() {
            if count == 0 { k = num }
            else if count == 1 { n = num }
            else {
                error(format!("{}: Too many elements in {{k,n}} list: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + 41), true);
            }
        }
        count += 1;
    }
    if k == 0 || n == 0 {
        error(format!("{}: Invalid {{k,n}} list: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = start + 28), true);
    }
    (k, n, p)
}


// 0: Invalid character to find pair for
// 1: Valid char, but no pair
fn try_find_pair(s: &str, i: usize) -> Result<usize, usize> {
    let a = match s.get(i..i + 1){
        Some(r) => r,
        None => return Err(0)
    };
    let b = if a == "(" {
        ")"
    } else if a == "[" {
        "]"
    } else if a == "{" {
        "}"
    } else {
        return Err(0);
    };
    let mut count = 0;
    for (j, c) in s.char_indices().skip(i) {
        if count < 0 {
            return Err(1)
        }
        if &c.to_string() == a {
            count += 1;
        }
        if &c.to_string() == b {
            count -= 1;
        }
        if j > i && count == 0 {
            return Ok(j);
        }
    }
    return Err(1);
}

fn find_pair(s: &str, i: usize, start: usize, orig: &str) -> usize {
    if let Ok(r) = try_find_pair(s, i) {
        r
    } else {
        error(format!("{}: Cannot find forward pair of character: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = i + start + 47), true);
        0
    }
}

fn ss<'a>(s: &'a str, begin: usize, end: usize, start: usize, orig: &str) -> &'a str {
    if let Some(r) = s.get(begin..end) {
        return r;
    }
    error(format!("{}: Expected characters beginning: \"{}\"\n{:width$}^", Red.bold().paint("fatal"), orig, "", width = begin + start + 40), true);
    ""
}
