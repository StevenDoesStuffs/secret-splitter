# secret_splitter

Takes a secret (like an encryption key) and does threshold secret sharing with mandatory parts and groups (recursion).

## Syntax

When splitting, a pattern is needed to determine how the secret will be split.

There are two "things" in the syntax, a node and a tree. Trees provide information on how to split the secret into parts, and nodes provide properties of the parts. A node is not a part, as it defines properties for multiple parts. A tree contains nodes and is passed through the command line as an argument when running the program.

The general tree syntax goes as follows: {_k_,_n_}[_elements_]

The secret is split into _n_ parts, _k_ of which will be needed to recover it. The _elements_ list is a list of nodes which will specify the properties of the parts.

Nodes can be one of four types: 

`a`: denotes `a` normal elements

`ma`: denotes `a` mandatory elements

`{k, n}[elements]`: denotes a tree

m{_k_,_n_}[_elements_]: denotes a mandatory tree

#### Syntax sugar

1) `{k, n}@` => `{k, n}[n]`

2) `{k, n}[elements]`, where `elements.len() < n` => `{k,n}[elements, a - elements.len()]`

3) `m` => `m1`

### Examples

`{2 ,4}[{3, 5}[m2], m]`: Splits the secret into 4 parts, two of which is needed to recover the secret. One of the parts is then split into 3 pieces, two of which are mandatory, three of which are needed to recover the secret. Another part is marked mandatory, and the rest are normal parts. Notice that syntax sugar #3 is applied, and so is #2 where the normal elements are implicit.

`{2,3}@`: Splits secret into 3 normal parts, two of which are needed to recover the secret.

`{2,3}[m{3,5}@]`: Splits the secret into 3 parts, 2 of which is needed to recover the secret, one of which is mandatory and split into 5 normal parts with 3 needed to recover the secret.

## Usage

### Base command

```
USAGE:
    secret_splitter [FLAGS] <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -r, --replace    Auto respond yes to override prompts
    -V, --version    Prints version information

SUBCOMMANDS:
    help       Prints this message or the help of the given subcommand(s)
    recover    Recovers your secret from parts
    split      Splits your secret`
```
### Recover
```
USAGE:
    secret_splitter recover [OPTIONS] --input <FOLDER> --output <FILE>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --dot <FILE>        Output file of dot graph
    -i, --input <FOLDER>    Folder where parts are contained
    -o, --output <FILE>     Output file
```
### Split
```
USAGE:
    secret_splitter split --file <FILE> --output <FOLDER> --pattern <PATTERN>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -f, --file <FILE>          File to split
    -o, --output <FOLDER>      Output parts to this folder
    -p, --pattern <PATTERN>    Splitting pattern. See gitlab for more info.
```

## Notes
1. The recover command will recursively loop through the input folder
1. Put the pattern in quotes if you encounter issues
3. The names of the parts don't matter, they are there to help you
4. The output of the split command will output files/folders with names that are numbers. If there is an m preceding the number, then the part is mandatory. If a folder is created, then it contains files within it.

## Building
As of writing, the rusty_secrets crate has weird dependency issues. As such, i've cloned it and put in the .dependeicies folder, and modified the cargo.toml to use an updated version of ring. There are also some dependency issues with merkle.rs, which rusty_secrets depends on which I also fixed. If you with to build the program, you must follow these steps.

1. Clone rusty_secrets and merkle.rs into .dependencies
2. Make sure protobufv2.5.1 is installed on your computer
3. Update the Cargo.toml of both the crates to use the latest version if ring
